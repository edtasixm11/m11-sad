# M11-SAD

## @edt ASIX M06 2021-2022

Podeu trobar les imatges docker al Dockehub de [edtasixm11](https://hub.docker.com/u/edtasixm11/)

Podeu trobar la documentació i els dokerfiles del mòdul a [ASIX-M11](https://github.com/edtasixm11)

Podeu trobar la documentació del mòdul a [ASIX-M11](https://sites.google.com/site/asixm11edt/)

[ASIX-M01](https://gitlab.com/edtasixm01/m01-operatius)  |  [ASIX-M05](https://gitlab.com/edtasixm05/m05-hardware) |  [ASIX-M06](https://gitlab.com/edtasixm06/m06-aso) |  [ASIX-M11](https://gitlab.com/edtasixm11/m11-sad)

---

### UF1 Sseguretat física, lògica i legislació

 * Permisos especials i ACLs
 * RAID
 * LVM
 * Backups
 * Certificats Digitals / GPG
 * FileSystem Crypt
 * Legislació

---

### UF2 Seguretat activa i accés remot

 * TCP-Wrappers
 * Kerberos
 * Tunnel SSH
 * VPN / IPSEC
 * Certificas Digitals /OpenSSL

---

### UF3 Tallafocs i servidors intermediàris

 * Firewall
 * Proxy

---

### UF4 Alta disponibilitat

 * Alta Disponibilitat (clustering)
 * Virtualització


