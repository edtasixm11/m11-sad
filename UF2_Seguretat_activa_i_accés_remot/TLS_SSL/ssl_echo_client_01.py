#! /usr/bin/python
# @edt 2016
# ASIX-M11 Seguretat
# SSL client: ECHO client
# **adaptat de python documentation**
# ------------------------------------------------------------------- 

import socket, ssl, pprint

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# require a certificate from the server
ssl_sock = ssl.wrap_socket(s, ca_certs="/var/tmp/openvpn/ca.crt", cert_reqs=ssl.CERT_REQUIRED)
ssl_sock.connect(('localhost', 50000))

print "-"*80
print repr(ssl_sock.getpeername())
print ssl_sock.cipher()
print pprint.pformat(ssl_sock.getpeercert())
print "-"*80
print s
print ssl_sock
print "-"*80

while True:
  dataUser = raw_input("-> ")
  if not dataUser: break
  ssl_sock.write(dataUser)
  dataServer = ssl_sock.read()
  print dataServer
ssl_sock.close()


