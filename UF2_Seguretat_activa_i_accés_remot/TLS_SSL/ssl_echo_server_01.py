#! /usr/bin/python
# @edt 2016
# ASIX-M11 Seguretat
# SSL server: ECHO server
# **adaptat de python documentation**
# ------------------------------------------------------------------- 

import socket, ssl, sys

bindsocket = socket.socket()
bindsocket.bind(('localhost', 50000))
bindsocket.listen(5)

def deal_with_client(connstream):
   print "SSL echo server (new connection)"
   print connstream
   print "-"*80
   data = connstream.read()
   while True:
      if not data: break
      connstream.write(data)
      data = connstream.read()
   # finished with client
   connstream.close()

while True:
   newsocket, fromaddr = bindsocket.accept()
   connstream = ssl.wrap_socket(newsocket, \
       server_side=True, certfile="/var/tmp/openvpn/server.crt", \
       keyfile="/var/tmp/openvpn/server.key", ssl_version=ssl.PROTOCOL_TLSv1)
   deal_with_client(connstream)
sys.exit(0)






